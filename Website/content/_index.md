---
title: CV
---
# Open Plant Project

A global knowledge-base of plant growth conditions and cultivation methods

[Join us on GitLab](https://gitlab.com/grownetics/open-plant-project)

{{% grid %}}

{{% column -span-cols-6 -m-right-2 %}}
## Summary

The Open Plant Project (OPP) is a collective effort between public and private institutions to create a world-wide crop database of cultivation methods and environmental data to collectively discover how to grow every plant optimally.

Today, farmers have relied on information passed down from earlier generations, local knowledge, university extension programs, and a handful of industry resources to share and learn about methods to improve quality, consistency, and yield. Often these resources are outdated or worse, based on unvalidated assumptions.

But, as farming rapidly evolves with the rise of cheaper and more efficient sensing and cultivation technologies it becomes harder and harder to keep up. Meanwhile universities have little access to real commercial data to support the validation of much of these technology claims. Couple these challenges with the urgent need to solve new food security issues arising from climate change and we have a real need for immediate solutions. This is where the Open Plant Project comes in, a multi-national project open to any cultivator worldwide.

In practice OpenPlantProject.org will host a free and open structured database of plant cultivation data where any cultivator can host, share, and cite each other's work in a world wide effort to build the most comprehensive knowledge-base of crop growth methods, conditions, and results. A lot of crop data already exists in university and government archives, it will be our job to not only act as a secondary archive, but ensure the continued discoverability and utility of these data sets. This open and structured data store will make it easy to do large studies across locales, crops, growing methodologies, and much more.

In an increasingly polluted world faced with climate change it is an equally important mission for us to address the challenges of inefficient and pesticide laden international food supply chains by supporting the growth of hyper efficient urban and small farming businesses. This is why there will be a focus on efficient cultivation techniques and best practices for growing clean, highly nutritious food and medicine at commercial scale.

To summarize, the open plant project is really a simple idea, for farmers and researchers to share crop growth data in support of learning how to grow every plant better, together.


{{% /column %}}

{{% column -span-cols-4 -p-left-3 %}}
#### Built for and by
  * Farmers
  * Gardeners
  * Growers
  * Hobbyists

{{% /column %}}
{{% /grid %}}
