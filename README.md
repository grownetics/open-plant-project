# Open Plant Project

The Open Plant Project (OPP)  is a collective effort between public and private institutions to create a world-wide crop database of cultivation methods and environmental data to collectively discover how to grow every plant optimally. 